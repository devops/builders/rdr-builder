#
# Dockerfile for building the RDR builder image.
#
FROM gitlab-registry.oit.duke.edu/devops/containers/ruby:2.6

ENV FITS_HOME  /opt/fits

USER 0

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install \
	clamdscan \
	ffmpeg \
	ghostscript \
	imagemagick \
	libreoffice \
	zip \
	# Curb - libcurl bindings for Ruby
	libcurl4-openssl-dev \
	# FITS dependencies
	file \
	mediainfo \
	openjdk-11-jre-headless \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

SHELL [ "/bin/bash", "-c" ]

COPY --from=gitlab-registry.oit.duke.edu/devops/containers/fits-container:1.5.0 /usr/src/fits $FITS_HOME

COPY ./etc /etc/

RUN set -eux; \
	# Symlink FITS script into PATH
	ln -s ${FITS_HOME}/fits.sh /usr/local/bin/fits.sh; \
	# /data volume
	mkdir -p /data; \
	chown -R ${APP_USER_UID}:0 /data; \
	# Smoke tests
	convert --version; \
	identify -list policy; \
	java -version; \
	echo "FITS $(fits.sh -v)"; \
	echo "Ghostscript $(gs --version)"; \
	soffice --version; \
	ffmpeg -version

VOLUME /data

USER $APP_USER_UID
