SHELL = /bin/bash

build_tag ?= rdr-builder

build:
	docker build --pull -t $(build_tag) .

test:
	docker run --rm $(build_tag) /bin/bash -c 'echo "Hello, world!"'

.PHONY: build test
